# Website Milla

## Checkout repository

```
#!

$ cd /home/username/Project/
$ git clone https://glennverschooren@bitbucket.org/glennverschooren/milla.git
$ cd milla
$ php composer(.phar) install (Don't do update! We need to read from the $.lock file)

```

## Setup milla in Apache2

```
NameVirtualHost milla:80

<VirtualHost milla:80>
ServerName milla
DocumentRoot /Users/peetes1/Sites/Milla/public
<Directory /Users/peetes1/Sites/Milla/public>
                DirectoryIndex index.php
                AllowOverride All
                Order allow,deny
                Allow from all
        </Directory>
</VirtualHost>

```

## Create a hosts file


```
#!

$ cd /etc/
$ sudo nano hosts
##
# Host Database
#
##

255.255.255     broadcasthost
#::1              localhost
#fe80::1%lo0    localhost

127.0.0.1       milla

```

# Front end setup

## Principle

* Gruntfile.js configures the tasks to build and optimize the assets
* package.json contains all NPM dependencies
* Gemfile + Gemfile.lock contains configuration settings for Compass, Sass, Singularitygs, Sass globbing, Breakpoint and Toolkit
* Bower.json contains all external libraries

## Installing

Prerequisites for installing the build tools are

* [nodejs](nodejs.org) (for grunt build environment)
* [ruby](ruby-lang.org)(for compiling SASS into CSS)

Ruby is present by default on Mac OS X. On most linux distros, it is either installed or available as a package. Check 'ruby -v' to see if it is installed (version 1.8.7 or 1.9.3 should work fine). Once ruby is installed, run the following command to install the dependencies: 'gem install bundler && bundle install'

# Setting up Grunt 

* Install NodeJs Globaly
* Install Bundler: (sudo) gem install bundler & bundler install
* Install Grunt-CLI global : (sudo) npm install -g grunt-cli
* Drag project folder in terminal and
* Install dependencies locally (project folder): (sudo) npm install

## Grunt Commands

* grunt -f: run this command to jshint and watch your sass files
* grunt build: run this command when building your application

# Setting up Bower

* Install Bower globaly: (sudo) npm install -g bower
* Install all packages: cd public && (sudo) bower install
<!DOCTYPE html>
<!--[if lt IE 8]>     <html lang="en" class="lt-ie7"><![endif]-->
<!--[if lt IE 9]>     <html lang="en" class="lt-ie8"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en" xmlns:ng="http://angularjs.org" id="ng-app"><!--<![endif]-->
<head>

	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!--
	DNS Prefetching resolves & caches domain names
	This results in faster download initialization
	- https://github.com/h5bp/html5-boilerplate/blob/master/doc/extend.md
	- http://daker.me/2013/05/5-html5-features-you-need-to-know.html

	<link rel="dns-prefetch" href="//cdnjs.cloudflare.com" />
	<link rel="dns-prefetch" href="//fonts.googleapis.com" />
	<link rel="dns-prefetch" href="//www.google-analytics.com" />
	-->


	<!-- SEO /////////////////////////////////////////////////////////////////////// -->

	<title> Theater Milla </title>

	<meta name="description" content="Theater Milla is een jong theatergezelschap uit Antwerpen dat in 2007 werd opgestart. op deze website kan je al onze producties terug vinden." />
	<meta name="keywords" content="theater, milla, theater milla, taal, cultuur, creatief, teksten schrijven, regiseren, acteren, livemuziek, decor, kostuums, woord, drama, beeldend" />

	<!--
	Add canonical url when dealing with duplicate content
	Signal to search engines to use the source URL in searchresults
	- http://support.google.com/webmasters/bin/answer.py?hl=nl&answer=139394&topic=2371375&ctx=topic
	- http://support.google.com/webmasters/bin/answer.py?hl=nl&answer=139066&topic=2371375&ctx=topic

	<link rel="canonical" href="http://www.|| URL ||/" />
	-->

	<!--
	Enforce search engines to use the page description
	Don't show the Google Translation popup toolbar
	http://support.google.com/webmasters/bin/answer.py?hl=nl&answer=79812&topic=2371375&ctx=topic
	-->

	<meta name="robots" content="noodp, noydir" />
	<meta name="google" content="notranslate" />


	<!-- MOBILE ////////////////////////////////////////////////////////////////////
	Set viewport on IOS, for RWD-projects this is needed to enable mediaQueries
	When added, don't prevent users from zooming unless there's a valid reason
	If the project is not RWD ommit this if possible

	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0, user-scalable=no, minimal-ui" />-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui" />

	<!--
	Display phone numbers as hyperlinks (IOS default: yes)
	http://www.html-5.com/metatags/index.html#format-detection-meta-tag
	-->

	<meta name="format-detection" content="telephone=no" />

	<!--
	Enable the page to run in standalone mode (android)
	Enable the page to run chromeless from the homescreen (IOS)
	- http://www.html-5.com/metatags/index.html#apple-mobile-web-app-capable
	- http://blog.teamtreehouse.com/optimizing-mobile-web-apps-ios

	<meta name="mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-title" content="|| TITLE ||" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	-->

	<!--
	Link to the Apple App Store
	- http://www.mobilexweb.com/blog/iphone-5-ios-6-html5-developers

	<meta name="apple-itunes-app" content="app-id=|| APPLE-APP-ID ||, app-argument=|| APP-DESCRIPTION ||" />
	-->

	<!--
	Let spiders know this page is optimized for mobile
	Set the viewport for IE Mobile
	Enable smooth font rendering on IE Mobile
	- http://www.metatags.nl/mobiele_metatags_voor_de_smartphone

	<meta name="HandheldFriendly" content="true" />
	<meta name="MobileOptimized" content="320" />
	<meta http-equiv="cleartype" content="on" />
	-->


	<!-- G+ AND FACEBOOK ///////////////////////////////////////////////////////////
	Added Facebook Share Button support
	- http://davidwalsh.name/facebook-meta-tags
	- http://ogp.me
	-->

	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="Theater Milla" />
	<meta property="og:title" content="Theater gezelschap Milla" />
	<meta property="og:description" content="Theater Milla is een jong theatergezelschap uit antwerpen dat in 2007 werd opgestart door vier enthousiaste jongeren" />
	<meta property="og:url" content="http://www.theater-milla.be" />
	<meta property="og:image" content="{{ asset('assets/img/gfxMilla/logo-black.png') }}" />
	<link rel="image_src"  content="{{ asset('assets/img/gfxMilla/logo-black.png') }}" />


	<!-- TWITTER CARDS /////////////////////////////////////////////////////////////
	Images for Twitter Cards: min. 280px width, and min. 150px height, max. 1MB
	Note: you need to validate & approve your cards once the site launches
	- https://dev.twitter.com/docs/cards/validation/validator
	-->

	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@|| TWITTER-ACCOUNT ||" />
	<meta name="twitter:title" content="|| TITLE ||" />
	<meta name="twitter:description" content="|| DESCRIPTION ||" />
	<meta name="twitter:image:src" content="http://www.|| URL ||/icons/750x500.jpg" />
	<meta name="twitter:image:width" content="750" />
	<meta name="twitter:image:height" content="500" />


	<!-- WINDOWS 8 TILES ///////////////////////////////////////////////////////////
	Tile icon for Win8 & disable link highlighting upon tap in IE10
	- http://blogs.msdn.com/b/ie/archive/2012/06/08/high-quality-visuals-for-pinned-sites-in-windows-8.aspx
	-->

	<meta name="application-name" content="|| SITE-NAME ||" />
	<meta name="msapplication-tooltip" content="|| DESCRIPTION ||" />
	<meta name="msapplication-TileImage" content="icons/152x152.png" />
	<meta name="msapplication-TileColor" content="#000000" />
	<meta name="msapplication-tap-highlight" content="no" />


	<!-- ICONS /////////////////////////////////////////////////////////////////////
	Favicon generator: http://favicon.htmlkit.com/favicon
	- http://www.netmagazine.com/features/create-perfect-favicon
	- https://developer.apple.com/library/mac/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html
	-->
	
	<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/img/icons/apple-touch-icon-57x57.png') }}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/img/icons/apple-touch-icon-60x60.png') }}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/img/icons/apple-touch-icon-72x72.png') }}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/icons/apple-touch-icon-76x76.png') }}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/img/icons/apple-touch-icon-114x114.png') }}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/img/icons/apple-touch-icon-120x120.png') }}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/img/icons/apple-touch-icon-144x144.png') }}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/img/icons/apple-touch-icon-152x152.png') }}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/img/icons/apple-touch-icon-180x180.png') }}">
	<link rel="icon" type="image/png" href="{{ asset('assets/img/icons/favicon-32x32.png') }}" sizes="32x32">
	<link rel="icon" type="image/png" href="{{ asset('assets/img/icons/android-chrome-192x192.png') }}" sizes="192x192">
	<link rel="icon" type="image/png" href="{{ asset('assets/img/icons/favicon-96x96.png') }}" sizes="96x96">
	<link rel="icon" type="image/png" href="{{ asset('assets/img/icons/favicon-16x16.png') }}" sizes="16x16">
	<link rel="manifest" href="{ asset('assets/img/icons/manifest.json') }}">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="{{ asset('assets/img/icons/mstile-144x144.png') }}">
	<meta name="theme-color" content="#ffffff">


	<!-- CSS ///////////////////////////////////////////////////////////////////////
	Mobile First setup, loading additional CSS as more screen estate is available
	- http://www.html5rocks.com/en/mobile/mobifying
	-->

	<link rel="stylesheet" href="{{ asset('assets/stylesheets/css/main.css') }}" />
	<link rel="stylesheet" href="{{ asset('assets/libs/slick-carousel/slick/slick.css') }}" />
	<link rel="stylesheet" href="{{ asset('assets/libs/ngDialog/css/ngDialog.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('assets/libs/ngDialog/css/ngDialog-theme-default.min.css') }}" />
	<!-- <link rel="stylesheet" href="{{ asset('assets/stylesheets/css/main-desktop.css') }}" media="screen and (min-width: 48em)" /> -->
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Libre+Baskerville:400,700" />

	<!--
	CSS fixes for < IE9
	Enable MediaQueries on IE7 + IE8 with respond.js
	Enable HTML5 elements on IE7 + IE8 (alternative for html5shiv)
	Note: > IE10 doesn't support conditional statements
	-->

	<!--[if (lt IE 9) & (!IEMobile)]>
		<link rel="stylesheet" href="css/main-ie.css" />
		<script src="js/libs/respond.min.js"></script>
		<script src="js/libs/html5-microshim.min.js"></script>
	<![endif]-->


	<!-- SILENT PRELOADING /////////////////////////////////////////////////////////
	Preload assets/pages after this page finished loading
	Prerendering fetches & renders entire pages at once
	- http://daker.me/2013/05/5-html5-features-you-need-to-know.html

	<link rel="prefetch" href=" SOME ASSET OR PAGE " />
	<link rel="prerender" href=" SOME PAGE " />
	-->

</head>
<body>

	<!-- NOSCRIPT MESSAGE //////////////////////////////////////////////////////////
	SPA (Single Page Applications) commonly rely on javascript being enabled
	If that's not the case, provide a feedback message to the user
	Note: Remove this when building onepagers/websites-->

	<noscript>

		Theater Milla requires javascript, which is not enabled in your browser.
		Here are instructions on <a href="http://www.enable-javascript.com/" target="_blank">how to enable javaScript</a> in your browser.

	</noscript>
	


	<!-- DEALING WITH < IE9 ////////////////////////////////////////////////////////
	Notify users they are not being served the best user experience
	Note: styles for this are applied in sass/main-ie.scss
	-->

	<!--[if lt IE 9]>
	<div id="outdated-browser-message">
		<p aria-hidden="true">
			Oops, it seems like you are using an outdated browser.
			<a href="http://browsehappy.com/?locale=en" target="_blank">Please upgrade</a> to improve your experience.
		</p>
	</div>
	<![endif]-->


	<!-- SEO & SCREEN READER SUPPORT ///////////////////////////////////////////////
	Add site/application information for SEO & screen readers
	-->

	<!--  <div class="any-surfer">

		<h1>Short about || SITE-NAME || title</h1>
		<p>|| DESCRIPTION ||</p>

	</div>-->


	<!-- LET'S FLOW ////////////////////////////////////////////////////////////////-->

	<section ui-view class="anim-in-out home clear" data-anim-sync="true" ></section>	


	<!-- SCRIPTS ///////////////////////////////////////////////////////////////////-->
	<script type="text/javascript"  src="{{ URL::asset('assets/libs/modernizr/modernizr.js') }}"></script>
	<script type="text/javascript"  src="{{ URL::asset('assets/libs/jquery/jquery.min.js') }}"></script>
	<script type="text/javascript"  src="{{ URL::asset('assets/libs/spin.js/spin.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/angular/angular.min.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/js/modules/angular-events.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/slick-carousel/slick/slick.min.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/angular-slick/dist/slick.min.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/angular-resource/angular-resource.min.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/angular-route/angular-route.min.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/angular-ui-router/release/angular-ui-router.min.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/angular-animate/angular-animate.min.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/angular-ripple/angular-ripple.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/angular-spinner/angular-spinner.min.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/angular-loading-bar/build/loading-bar.min.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/ng-lodash/build/ng-lodash.min.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/angular-scroll/angular-scroll.min.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/angular-scroll/angular-scroll.min.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/ngDialog/js/ngDialog.min.js') }}"></script>
    <script type="text/javascript"  src="{{ URL::asset('assets/libs/Snap.svg/dist/snap.svg-min.js') }}"></script>

    <script type="text/javascript"  src="{{ URL::asset('assets/js/main.js') }}"></script>


    <!-- CONTROLLERS ///////////////////////////////////////////////////////////////////-->

  	<script type="text/javascript"  src="{{ URL::asset('assets/js/controllers/homeController.js') }}"></script>
  	<script type="text/javascript"  src="{{ URL::asset('assets/js/controllers/ticketController.js') }}"></script>
  	<script type="text/javascript"  src="{{ URL::asset('assets/js/controllers/infoController.js') }}"></script>


  	<!-- DIRECTIVES CONTROLLERS ///////////////////////////////////////////////////////////////////-->
  	<script type="text/javascript"  src="{{ URL::asset('assets/js/directives/controllers/inputfield.js') }}"></script>


  	<!-- ANIMATIONS ///////////////////////////////////////////////////////////////////-->

  	<script type="text/javascript"  src="{{ URL::asset('assets/js/animation/slide.js') }}"></script>

  	<!-- FACTORIES ///////////////////////////////////////////////////////////////////-->
	
	<script type="text/javascript"  src="{{ URL::asset('assets/js/factories/svg-icons.js') }}"></script>

	<!-- SERVICES ///////////////////////////////////////////////////////////////////-->
	
	<script type="text/javascript"  src="{{ URL::asset('assets/js/services/svgIconConfig.js') }}"></script>
	<script type="text/javascript"  src="{{ URL::asset('assets/js/services/helper.js') }}"></script>
	<script type="text/javascript"  src="{{ URL::asset('assets/js/services/validation.js') }}"></script>
	<script type="text/javascript"  src="{{ URL::asset('assets/js/services/form.js') }}"></script>

	<!-- DIRECTIVES ///////////////////////////////////////////////////////////////////-->
	<script type="text/javascript"  src="{{ URL::asset('assets/js/directives/nav.js') }}"></script>
	<script type="text/javascript"  src="{{ URL::asset('assets/js/directives/tabs.js') }}"></script>
	<script type="text/javascript"  src="{{ URL::asset('assets/js/directives/pane.js') }}"></script>
	<script type="text/javascript"  src="{{ URL::asset('assets/js/directives/form.js') }}"></script>
	<script type="text/javascript"  src="{{ URL::asset('assets/js/directives/inputfield.js') }}"></script>


	<script type="text/ng-template" id="inputfield-default.htm">
		<div class="inputWrapper <% layout.fieldClass %>" data-ng-class="{labelHidden: label.length === 0, noValidation: validation === undefined}">
			<label data-ng-show="label.length > 0" data-ng-class="{required: validation.required != undefined && validation.required}" for="<% id %>"><% label %></label>
			<span data-ng-show="label.length == 0" data-ng-class="{required: validation.required != undefined && validation.required}"></span>
			<div class="inputLayoutWrapper">
				<input
					class="<% fieldStyle %>"
					data-ng-class="{fieldValid: state.isValid, fieldInvalid: !state.isValid, 'ng-dirty': state.dirty, 'ng-pristine': !state.dirty}"
					id="<% id %>"
					name="<% name %>"
					type="<% type %>"
					tabindex="<% tabindex %>"
					data-ng-model="ngModel"
					placeholder="<% placeholder %>"
					data-ng-change="onChange()"
					data-ng-click="onClick($event)"
					data-ng-focus="onFocus($event)"
					data-ng-blur="onBlur($event)"
					data-ng-readonly="readonly"
					data-ng-disabled="disabled" />
					<div class="field-problem" data-ng-show="(errorMessage.length > 0 && !state.isValid && state.hasBeenValid) || forceError" data-ng-cloak>
						<p data-ng-if="!state.isValid"><span><% errorMessage %></span></p>
						<p data-ng-if="state.isValid && forceError"><span><% forceError %></span></p>
					</div>

				</div>
			</div>
		</div>
	</script>


	<script type="text/ng-template" id="inputfield-select.htm">
		<div class="inputWrapper <% layout.fieldClass %>" data-ng-class="{labelHidden: label.length == 0, noValidation: validation == undefined}">
			<label data-ng-show="label.length > 0" data-ng-class="{required: validation.required != undefined && validation.required}" for="<% id %>"><% label %></label>
			<span data-ng-show="label.length == 0" data-ng-class="{required: validation.required != undefined && validation.required}"></span>
			<div class="select-wrapper">
				<select class="<% fieldStyle %>" id="<% id %>" name="<% name %>" data-ng-model="ngModel" data-ng-options="o.key as o.value for o in ngOptions" data-ng-change="onChange()" data-ng-click="onClick($event)" data-ng-focus="onFocus($event)" data-ng-blur="onBlur($event)" data-ng-disabled="disabled || state.printLoadingText" data-ng-class="{'select-mini': isMini}">

					<option value="" disabled="disabled">
						<% state.printLoadingText ? (loadingplaceholder || "Laden...") : placeholder %>
					</option>
				</select>
				<div class="field-problem" data-ng-show="(errorMessage.length > 0 && !state.isValid && state.hasBeenValid) || forceError">
					<p data-ng-if="!state.isValid"><span><% errorMessage %></span></p>
					<p data-ng-if="state.isValid && forceError"><span><% forceError %></span></p>
				</div>
			</div>
		</div>
	</script>


	<!-- GOOGLE ANALYTICS //////////////////////////////////////////////////////////
	Async Google Analytics snippet - change UA-XXXXX-X to your site ID
	https://developers.google.com/analytics/devguides/collection/gajs/?hl=nl -->

	<script>
		var _gaq=[['_setAccount','UA-59989649-1'],['_trackPageview']];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
		g.src='//www.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
	</script>


</body>
</html>
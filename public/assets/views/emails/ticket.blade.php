<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" align="center" width="100%">
    <tr>
        <td valign="top">
            <table cellpadding="0" cellspacing="0" border="0" align="center" id="wrapperTable">
                <tr>
                    <td valign="top" align="center" width="100%">
                        <table cellpadding="0" cellspacing="0" border="0" align="center" class="sixHundredFortyTable" width="600">

                            <!-- HEADER -->
                             <tr>
                                <td class="sixHundredFortyTD" width="100%" valign="top" align="center"><h1 style="margin: 0; padding: 0;font-size: 40px; font-weight: 300; color: #333333; font-family: 'Open Sans', Arial, sans-serif; ">Tickets Theater Milla</h1></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <!-- END HEADER -->

                            <!-- BODY -->
                            <tr>
                                <td class="sixHundredFortyTD" valign="top" align="left" style="font-size: 13px; color: #666666; font-weight: 300; font-family: 'Open Sans', Arial, sans-serif;">
                                    <p>Aanvraag Tickets,</p>

                                    <p style="text-align: left; margin-bottom: 5px;"><strong>Persoonlijke gegevens</strong></p>

                                    <table style="width:100%; border: 1px solid black; border-collapse: collapse">
                                        <tr>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: left;">Voornaam</th>
                                            <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;">{{ $fields['data']['Voornaam'] }}</td>
                                        </tr>
                                        <tr>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: left;">Naam</th>
                                            <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;">{{ $fields['data']['Naam'] }}</td>
                                        </tr>
                                        <tr>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: left;">Datum</th>
                                            <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;">{{ $fields['data']['Datum'] }}</td>
                                        </tr>
                                        <tr>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: left;">E-mail</th>
                                            <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;">{{ $fields['data']['E-mail'] }}</td>
                                        </tr>
                                    </table>
                                    <br>
                                    <br>
                                    <p style="text-align: left; margin-bottom: 5px;"><strong>Bestelling</strong></p>
                                    <table style="width:100%; border: 1px solid black; border-collapse: collapse">
                                        <tr>
                                            <th></th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; align: left;">aantal</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; align: left;">eenheidsprijs in euro</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; align: left;">bedrag in euro</th>
                                        </tr>
                                        <tr>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Tickets studentenprijs</th>
                                            <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;">{{ $fields['data']['Aantal-26j'] }}</td>
                                            <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"> 7 euro</td>
                                            <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;">{{  $fields['price']['totalAmountMin26'] }} euro</td>
                                        </tr>
                                        <tr>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Tickets basisprijs</th>
                                            <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;">{{ $fields['data']['Aantal+26j'] }}</td>
                                             <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"> 10 euro</td>
                                            <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;">{{  $fields['price']['totalAmountPlus26'] }} euro</td>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <td></td><td></td><td style="border-collapse: collapse; padding: 5px;"><strong> totaal bedrag: {{  $fields['price']['totalAmount'] }} euro</strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

# Change from ':development' to ':production' when needed.
environment = :development

# Default to development if environment is not set.
saved = environment
if (environment.nil?)
  environment = :development
else
  environment = saved
end


require 'breakpoint'
require 'singularitygs'
require 'sass-globbing'
require 'toolkit'

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "stylescheets/css"
images_dir = "img"
sass_dir = "stylescheets/sass"

# You can select your preferred output style here (:expanded, :nested, :compact
# or :compressed).
output_style = (environment == :production) ? :compressed : :compact

# Conditionally enable relative assets.
relative_assets = true

# Conditionally enable line comments when in development mode.
line_comments = (environment == :production) ? false : false

# Output debugging info in development mode.
sass_options = (environment == :production) ? {} : {:debug_info => false}

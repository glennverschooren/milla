

var milla = angular.module('milla', ['ngRoute', 'ngLodash', 'ui.router', 'ngResource', 'ngEvents', 'ngAnimate', 'chieffancypants.loadingBar', 'slick', 'angularRipple', 'duScroll', 'angularSpinner', 'ngDialog']);

milla.config([
	'$sceDelegateProvider',
	'$controllerProvider',
	'$routeProvider',
	'$stateProvider',
	'$filterProvider',
	'$provide',
	'$interpolateProvider',
	'$locationProvider',
	'$httpProvider',
	'cfpLoadingBarProvider',
	function AppConfig($sceDelegateProvider, $controllerProvider, $routeProvider, $stateProvider, $filterProvider, $provide, $interpolateProvider, $locationProvider, $httpProvider, cfpLoadingBarProvider) {
		// Loading bar config
		cfpLoadingBarProvider.includeSpinner = false;

		// prepare for dynamic loading
		milla.controllerProvider = $controllerProvider;
		milla.routeProvider = $routeProvider;
		milla.filterProvider = $filterProvider;
		milla.provide = $provide;

		$interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');

		$stateProvider.state('home', {
			url: '/home',
			templateUrl: '../assets/js/views/home.html',
			controller: 'milla.HomeController'
		}).state('homeRoot', {
			url: '/',
			templateUrl: '../assets/js/views/home.html',
			controller: 'milla.HomeController'
		});

		// When routing between internal front-end routes, one can now use pushstate, a HTML5 feature
		// to remove the old URIs with a `#` character. Now you can link to `www.domain.com/profile` instead
		// of `www.domain.com/#/profile`.
		$locationProvider.html5Mode({
			enabled: true,
			requireBase: false
		});

		// When falling back on a hashed url, it's best to prefix it with a bang `!`,
		// this gives us the ability to still use hashes for anchors.
		$locationProvider.hashPrefix('!');
	}
]);

function hasModule(moduleName) {
	try {
		return angular.module(moduleName);
	} catch (e) {
		return false;
	}
}


milla.run([
	'$rootScope',
	function AppRun($rootScope) {
		$rootScope.state = {};
	}
]);


function bootstrap() {
	if (window.angular && window.$ && angular.bootstrap && hasModule('milla')) {
		$(function () {
			// use documentElement instead of document to allow rightclick in firefox
			angular.bootstrap(document.documentElement, ['milla']);
		});
	}
}

bootstrap();
(function InputFieldDirective(ng, milla) {
	'use strict';

	milla.directive('inputField', [
		'lodash',
		'$timeout',
		'$compile',
		'$templateCache',
		'$http',
		'eventsService',
		'ValidationService',
		function InputFieldDerictive(_, $timeout, $compile, $templateCache, $http, Events, Validation) {

			return {
				restrict: 'A',
				require: '?ngModel',
				scope: {
					ngModel: '=?',
					type: '@',
					id: '@',
					autofocus: '=?',
					label: '@?',
					layout: '=?',
					name: '@',
					ngChange: '=?',
					ngClick: '=?',
					ngFocus: '=?',
					placeholder: '@?',
					validation: '=?',
					readonly: '=?',
					disabled: '=?',
					ngOptions: '=?',
					lazyload: '=?',

				},
				controller: 'InputFieldController',
				link: function link(scope, element, attrs, ctrl) {

					var modelWatch, placeholder = '', watchers = [], parentform = '';

					function checkLazyload() {
						if (scope.ngModel === undefined) {
							if (scope.lazyload) {
								modelWatch = scope.$watch('ngModel', function onModelChange() {
									if (scope.ngModel !== undefined) {
										initialize();
										modelWatch();
									}
								});
							}
						} else {
							initialize();
						}
					}

					//
					// INITIALIZE -------------------------------------------------
					//

					function initialize() {

						switch (scope.type) {
					
						case "select":
							if (!scope.ngOptions) {
								if (scope.lazyload) {
									scope.state.printLoadingText = true;
									placeholder = scope.placeholder || 'Maak een keuze';
									scope.placeholder = scope.loadingPlaceholder || 'Laden...';
									watchers.push(scope.$watch('ngOptions', function onScopeChange(newValue, oldValue) {
										var valueChangedAndOptionsNotUndefinedAndOneOrMoreOptions = (newValue !== oldValue && newValue !== undefined && newValue.length > 0);

										scope.state.printLoadingText = !valueChangedAndOptionsNotUndefinedAndOneOrMoreOptions;

										if (valueChangedAndOptionsNotUndefinedAndOneOrMoreOptions) {
											scope.placeholder = placeholder;
											validateInput(false);
										}

									}));
									validateInput(true);
								} else {
									console.error('The input directive with type [%s] requires at least 1 option.', scope.type);
									return;
								}
							}
							break;
						}

						var readOnlyOrDisabled = checkReadOnlyOrDisabled();

						if (!readOnlyOrDisabled) {
							if (String(scope.ngModel).length > 0) {
								setDirty(element);
								//validateInput(true);
							}
							else
							{
								//validateInput(false);
							}
							checkParentForm();

							// check autofocus & matches
							checkAutofocus();
						}
						
					}

					//
					// CHECK READONLY OR DISABLED------------------------------------------
					//
					// check wether the readonly or disabled properties were set
					//

					function checkReadOnlyOrDisabled() {
						if (scope.readonly || scope.disabled) {
							return true;
						}

						return false;
					}

					//
					// SET DIRTY -----------------------------------------------------------
					// set the input to dirty to enable visual validation of prefilled fields
					//

					function setDirty(elm) {
						ctrl.$pristine = false;
						ctrl.$dirty = true;
						scope.state.dirty = true;

						if (scope.type === 'date') {
							scope.state.hasBeenValid = true;
						}

					}


					//
					// CHECK PARENT FORM ---------------------------------------------------
					//

					function checkParentForm() {
						// subscribe to the parent forms submission
						Events.subscribe('form.submit', function onFormSubmit(form) {
							parentform = $('#' + scope.id).closest('form').attr('name') || '';
							if (form === parentform) {
								scope.state.hasBeenValid = true;
								scope.state.dirty = true;
								// we need to check if the inputfield are valid
								validateInput(true);
							}
						}, scope);
					}

					//
					// CheckAutofocus -------------------------------------------------------
					// check if the autofocus attribute was set, focus the input
					//

					function checkAutofocus() {
						if (scope.autofocus) {
							$timeout(function () {
								$('#' + scope.id).focus();
							}, 0);
						}
					}

					/* DATE */

					function toggleDatepicker() {
						if (scope.pickerMode.picker) {
							scope.state.picker = !scope.state.picker;

							if (scope.state.picker) {
								scrollToDatePicker();
								$timeout(function () {
									$('body').on('click', function onClick(e) {
										if ($(e.target).hasClass('datepicker') || $(e.target).parents(".datepicker").size()) {
											return;
										}

										clearDatePickerEvents();
									});

									$('body').on('keydown', function onKeyDown(e) {
										if (e.which === 27) {
											clearDatePickerEvents();
										}
									});
								});
							}
							else {
								element.off('keydown');
								$('body').off('click');
							}
						}
					}

					function clearDatePickerEvents() {
						$timeout(function () {
							scope.state.picker = false;
							element.off('keydown');
							$('body').off('click').off('keydown');
						});
					}

					function toggleTimepicker() {
						scope.state.timePicker = !scope.state.timePicker;
					}

					function scrollToDatePicker() {
						// Scroll the picker in the viewport
						$timeout(function () {
							var datePickerElement = $('#' + scope.id).parent().parent().find(".datepicker");
							var destination = $('body').scrollTop() + datePickerElement.height();

							$('html,body').animate({ scrollTop: destination}, 250);
						}, 250);
					}

					function pickedDate(value) {
						scope.state.picker = false;
						scope.ngModel = value;

						scope.onChange();
						if (!scope.$$phase) {
							scope.$apply();
						}
					}

					function pickedTime(value) {
						scope.ngModel = value;
						scope.onChange();
					}


					//
					// SCOPE VARIABLES ---------------------------------------------
					//

					scope.state = {
						warning: false,
						critical: false,
						isValid: false,
						hasBeenValid: false,
						picker: false,
						dirty: false,
						printLoadingText: false
					};

					scope.toggleDatepicker = toggleDatepicker;
					scope.pickedDate = pickedDate;
			

					//
					// SCOPE FUNCTIONS ---------------------------------------------
					//


					//
					// VALIDATE INPUT ---------------------------------------------
					//
					// Update the scope.state object depending on the validation
					// 

					function validateInput(validationVisual) {

						var isValid = {
							valid: true
						};

						validationVisual = validationVisual !== undefined ? validationVisual : true;

						if (scope.validation !== undefined) {
							isValid = Validation.validateInput(scope.validation, scope.ngModel, scope.type);
						}

						if (isValid.valid) {

							ctrl.$setValidity('valid', true);
							if (validationVisual) {
								scope.state.isValid = true;
								scope.state.hasBeenValid = true;
								//console.log('state is valid', scope.state);
							}

						} else {
							ctrl.$setValidity('valid', false);
							if (validationVisual) {
								scope.state.isValid = false;
								scope.errorMessage = isValid.message;
								//console.log('state is not valid', scope.state);
							}
						}
					}


	
					//
					// Bind to the scope events -------------------------------------
					//

					scope.onChange = function onChange(value) {
						// check if the inputfield is still valid
						// validate input field
						switch (scope.type) {
						case 'date':
							scope.state.hasBeenValid = true;
							clearDatePickerEvents();
							validateInput();
							break;
						default:
							validateInput();
						}

					};

					scope.onFocus = function onFocus(event) {
						if (scope.ngFocus !== undefined) {
							scope.ngFocus(event);
						}
					};

					scope.onClick = function onClick(event) {
						if (scope.ngClick !== undefined) {
							scope.ngClick(event);
						}
					};

					scope.open = function ($event) {
						$event.preventDefault();
						$event.stopPropagation();

						scope.opened = true;
					};

					scope.dateOptions = {
						formatYear: 'yy',
						startingDay: 1
					};

					scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
					scope.format = scope.formats[0];


					//
					// BIND TO EVENTS -----------------------------------------------
					//
					

					watchers.push(scope.$watch('disabled', function onScopeChange(newValue, oldValue) {
						var valueChangedAndNotReadOnlyAndNotDisabled = (newValue !== oldValue && !scope.readonly && !newValue);
						if (valueChangedAndNotReadOnlyAndNotDisabled) {
							checkParentForm();
						}
					}));

					watchers.push(scope.$watch('readonly', function onScopeChange(newValue, oldValue) {
						if (newValue !== oldValue && !newValue && !scope.disabled) {
							checkParentForm();
						}
					}));

					watchers.push(scope.$watch('validation', function onScopeChange(newValue, oldValue) {
						if (!angular.equals(newValue, oldValue) && !scope.disabled) {
							validateInput(true);
							setDirty(element);
						}
					}, true));

					scope.$on('$destroy', function onScopeDestroy() {
						_.each(watchers, function (watcher) {
							watcher();
						});
					});
					
					// --- Initialize. ---------------------------------- //
					
					checkLazyload();
				}
			};
		}
	]);
})(angular, milla);

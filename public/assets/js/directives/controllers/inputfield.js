(function InputFieldController(ng, milla) {
	'use strict';

	milla.controller('InputFieldController', [
		'$scope',
		'$element',
		'$http',
		'$templateCache',
		'$compile',
		'$rootScope',
		'lodash',
		function InputFieldController($scope, $element, $http, $templateCache, $compile, $rootScope, _) {

			
			//
			// CONTROLLER VARIABLE --------------------------------------------------------
			//

			var type = '',
				pseudoTypes = [],
				defaultLayout = {
					fieldClass: 'span-full tablet--span-half-1 desktop--span-half-1'
				};


			//
			// GET TEMPLATE ---------------------------------------------------------------
			// get the correct template and return the call to the templatecache
			//


			function getTemplate(type) {
				var itemTemplate;
				
				switch (type) {
				case 'date':
				case 'select':
				case 'textarea':
					itemTemplate = 'inputfield-' + type + '.htm';
					break;
				default:
					itemTemplate = 'inputfield-default.htm';
					break;
				}

				//var templateLoader = $http.get(itemTemplate, {cache: $templateCache});
				var templateLoader = $templateCache.get(itemTemplate);

				return templateLoader;
			}


			//
			// INITIALIZE --------------------------------------------------------------------
			//

			function initialize() {
				// stop execution if no type was provided
				if ($scope.type === undefined) {
					console.error("The input field directive requires a type.");
					return false;
				}

				type = checkType($scope.type);

				// if no model was provided, check the lazyload attribute
				// if it is set, watch the model for changes, else stop execution
				if ($scope.ngModel === undefined && !$scope.lazyload) {
					console.error("The input field directive requires a model (" + $scope.name + ").");
					return false;
				}

				if (checkAttrs()) {
					setDefaults();
					renderInputField();
				}
			}


			//
			// CHECK ATTRS ---------------------------------------------------------------------
			// check if the required attributes for the inputfield are present
			//

			function checkAttrs() {
				switch (type) {
				case 'select':
					// If lazyloading is true -> watch the options
					if ($scope.ngOptions === undefined || $scope.ngOptions.length < 1) {
						if (!$scope.lazyload) {
							console.error("The input field directive with type *RADIO* and *CHECKBOXLIST* require at least one choice in the choices model.");
							return false;
						} else {
							return true;
						}
					}
					return true;
				default:
					if ($scope.id === undefined || $scope.name === undefined) {
						console.error('The input type results in default, and therefore requires an `id` [%s], a `name` [%s] and `ngModel` [%s]', $scope.id, $scope.name, typeof ($scope.ngModel));
						return false;
					}

					return true;
				}
			}



			//
			// SET DEFAULTS ----------------------------------------------------------------------
			// set the default values for all attributes
			//

		
			function setDefaults() {
				$scope.label = $scope.label || '';
				$scope.placeholder = $scope.placeholder || '';
				$scope.description = $scope.description || '';
				$scope.delay = $scope.delay || 0;
				$scope.layout = $scope.layout !== undefined ? $scope.layout : defaultLayout;
				$scope.isMini = $scope.isMini !== undefined ? $scope.isMini : false;
				$scope.errorMessage = $scope.validation !== undefined ? $scope.validation.errorMessage : '';
				$scope.readonly = ($scope.readonly !== undefined) ? $scope.readonly : false;
				$scope.disabled = ($scope.disabled !== undefined) ? $scope.disabled : false;

				// @todo: parse zend forms in helper service to avoid this crap
				$scope.ngModel = $scope.ngModel === null ? "" : $scope.ngModel;

				switch ($scope.type) {
				case 'select':
					$scope.placeholder = $scope.placeholder || 'Maak een keuze…';
					break;
				case 'date':
					$scope.pickerMode = {
						picker: true,
						input: true
					};
					if ($scope.datepickerMode !== undefined) {
						switch ($scope.datepickerMode) {
						case 'full':
							$scope.pickerMode.picker = true;
							$scope.pickerMode.input = true;
							break;
						case 'inputOnly':
							$scope.pickerMode.picker = false;
							$scope.pickerMode.input = true;
							break;
						case 'pickerOnly':
							$scope.pickerMode.picker = true;
							$scope.pickerMode.input = false;
							break;
						}
					}
					break;
				}
			}


			//
			// RENDER INPUTFIELD  ----------------------------------------------------------------------
			// Render the imputfield with his final template 
			//

			function renderInputField() {
				var html, promise, el;

				html = getTemplate(type);
				/*promise = loader.success(function onLoaderSuccess(html) {
					console.log(html);
					el = $compile(html)($scope);
					var replaceElement = $element.find('#inputFieldReplace');
					replaceElement.replaceWith(el);
					//$element.replaceWith(el);
				});*/

				el = $compile(html)($scope);
				$element.replaceWith(el);


			}


			//
			// CHECK TYPE -------------------------------------------------------------------------------
			// check for pseudotypes
			//

			function checkType(type) {
				if (pseudoTypes.indexOf(type) > -1) {
					return "text";
				}

				return type;
			}


			// initialize 

			initialize();

		}
	]);
})(angular, milla);

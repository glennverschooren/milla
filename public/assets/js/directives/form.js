(function formDirective(ng, milla) {
	'use strict';

	milla.directive('form', [
		'lodash',
		'eventsService',
		function formWrapper(_, eventsService) {
			return {
				restrict: 'E',
				compile: function (element, attrs) {
					return function (scope, element, attrs, controller) {
						// --- Define Controller Methods. ------------------- //
						function initialize() {
							// Fix autofill issues where Angular doesn't know about autofilled inputs
							if (attrs.ngSubmit) {
								setTimeout(function () {
									element.unbind('submit').submit(function (e) {
										e.preventDefault();

										var inputs = element.find('input, textarea, select');
										_.each(inputs, function (input) {

											// don't trigger a change event on file upload fields
											// otherwise we call the upload xhr twice!
											// In this case the user will think that they did upload a file!
											var type = $(input).attr('type');
											if (type !== 'file') {
												$(input).trigger('input').trigger('change').trigger('keydown');
											}
										});

										//when form is posted, publish event
										// eventsService.publish('form.posted');
										eventsService.publish('form.submit', attrs.name);

										scope.$apply(attrs.ngSubmit);
									});
								}, 0);
							}
						}

						// --- Define Scope Methods. ------------------------ //

						// --- Bind To Scope Events. ------------------------ //

						// --- Initialize. ---------------------------------- //
						initialize();
					};

				}
			};
		}
	]);
})(angular, milla);
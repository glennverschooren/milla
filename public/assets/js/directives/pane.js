(function PaneDirective(ng, milla) {
	'use strict';

	milla.directive('pane', [
		'lodash',
		function Pane(_) {

			return {
				restrict: 'A',
				require: '^tabs',
				transclude: true,
				scope: {
					title: '@',
					template: '=',
					info: '=?'
				},
				templateUrl: 'assets/js/views/directives/pane.html',
				link: function (scope, element, attrs, tabsCtrl) {

					// add a new pane to the tabs
					tabsCtrl.addPane(scope);

					/**
					 * Toggle up down ---------------------------------
					 * this toggles the wrapper div up and down 
					 * on the same moment we change the template
					 */
					function toggleUpDow() {
						$(element).slideUp(300);
						$(element).slideDown(300);
					}


					/**
					 * Watch the selected scope variable -------------
					 * 
					 * @param  {String} 'newvalue' new value
					 * @param  {String} 'oldvalue' old value
					 */
					scope.$watch('selected', function (newvalue, oldvalue) {

						if (newvalue !== oldvalue) {
							toggleUpDow();
						}
					});
				}
			};
		}
	]);
})(angular, milla);
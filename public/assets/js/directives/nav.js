(function NavDirective(ng, milla, $) {
	'use strict';

	milla.directive('nav', [
		'lodash',
		'SvgIconConfig',
		'SvgIcons',
		function Nav(_, SvgIconConfig, SvgIcons) {

			return {
				restrict: 'A',
				transclude: true,
				scope: {
					menuItems: '='
				},
				replace: true,
				templateUrl: 'assets/js/views/directives/nav.html',
				link: function (scope, element, attrs) {


					/**
					 * Initialize ------------------------------
					 */
					function initialize() {
						scope.toggle = {
							isOpen: false
						};
						var icon = $(element).find('.si-icon-home-cross');
						scope.icon = new SvgIcons($(icon), SvgIconConfig.data);
					}


					/**
					 * Set active ------------------------------
					 */
					scope.setActive = function setActive() {
						scope.toggle.isOpen = !scope.toggle.isOpen;
						scope.icon.toggle(true);
					};

					initialize();
				}
			};
		}
	]);
})(angular, milla, jQuery);

(function TabsDirective(ng, milla) {
	'use strict';

	milla.directive('tabs', [
		'lodash',
		function Tabs(_) {

			return {
				restrict: 'A',
				transclude: true,
				scope: {},
				templateUrl: 'assets/js/views/directives/tabs.html',
				controller: function ($scope) {
					var panes = $scope.panes = [];

					/**
					 * Select ---------------------------------
					 * when we select a tab we must activate a pane
					 * @param  {Object} 'pane' the data from the pane
					 */
					$scope.select = function (pane) {
						angular.forEach(panes, function (pane) {
							pane.selected = false;
						});
						pane.selected = true;
					};

					/**
					 * addPane --------------------------------
					 * @param {Object} pane [description]
					 */
					this.addPane = function (pane) {
						if (panes.length === 0) {
							$scope.select(pane);
						}
						panes.push(pane);
					};
				}
			};
		}
	]);
})(angular, milla);

(function EventsService(ng) {
	'use strict';

	/* pubsub - based on https://github.com/phiggins42/bloody-jquery-plugins/blob/master/pubsub.js*/
	var eventsService = function eventsService($q, $rootScope) {
		var cache = {};

		var unsubscribe = function unsubscribe(handle) {
			var t = handle[0];

			if (cache[t]) {
				$.each(cache[t], function (idx) {
					if (this === handle[1]) {
						cache[t].splice(idx, 1);
					}
				});
			}
		};

		var publish = function publish() {
			var args = [].splice.call(arguments, 0);
			var topic = args[0];
			var deferred = $q.defer();
			args.splice(0, 1);

			if (cache[topic]) {
				$.each(cache[topic], function () {
					var callback = this;
					if (topic.indexOf('socket.') === 0) {
						$rootScope.$apply(function () {
							callback.apply(null, args || []);
						});
					} else {
						callback.apply(null, args || []);
					}

				});
				deferred.resolve.apply(null, args);
			} else {
				deferred.resolve.apply(null, args);
			}

			return deferred.promise;
		};

		var subscribe = function subscribe(topic, callback, scope) {
			if (!cache[topic]) {
				cache[topic] = [];
			}
			cache[topic].push(callback);

			var handle = [topic, callback];

			if (scope !== undefined) {
				scope.$on("$destroy", function onScopeDestroy() {
					unsubscribe(handle);
				});
			}

			return handle;
		};

		return {
			// default methods
			publish: publish,
			subscribe: subscribe,
			unsubscribe: unsubscribe,

			// aliasses for subscribe
			on: subscribe,

			// aliasses for unsubscribe
			off: unsubscribe,

			// aliasses for publish
			emit: publish
		};
	};

	angular.module('ngEvents', []).factory('eventsService', eventsService);

})(angular);


(function InfoController(ng, milla) {
	'use strict';

	milla.controller('milla.infoController', [
		'$scope',
		'ngDialog',
		'lodash',
		function InfoController($scope, ngDialog, _) {

			/**
			 * Initialize -------------------------------------
			 * @return {[type]} [description]
			 */
			function initialize() {
				
			}

			$scope.openPoster = function openPoster() {
				ngDialog.open({
					template: '/assets/js/views/directives/dialog.html',
					scope: $scope
				});
			};

			initialize();

		}
	]);


})(angular, milla);

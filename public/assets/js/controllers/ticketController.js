
(function TicketController(ng, milla) {
	'use strict';

	milla.controller('milla.ticketController', [
		'$scope',
		'formService',
		'usSpinnerService',
		'lodash',
		function TicketController($scope, formService, usSpinnerService, _) {


			
			/**
			 * Initialize -------------------------------------
			 * @return {[type]} [description]
			 */
			function initialize() {
				
			}

			$scope.formState = {
				isBusy: false
			};

			$scope.step = 1;


			/**
			 * PREV STEP --------------------------------------
			 * gop to the previous step 
			 */
			$scope.prevStep = function prevStep() {
				$scope.step = 1;
			};

			/**
			 * Submit form -----------------------------------------
			 * @return {[type]} [description]
			 */
			$scope.submitForm = function submitForm() {

				
				$scope.techErrors = [];
				
				if ($scope.ticketForm.$valid) {
					$scope.formState.isBusy = true;
					usSpinnerService.spin('milla-spinner');

					formService.sendForm($scope.info.form, function success(response) {
						$scope.formState.isBusy = false;
						usSpinnerService.stop('milla-spinner');


						$scope.step = 2;

					}, function onError(err) {
						$scope.formState.isBusy = false;
						usSpinnerService.stop('milla-spinner');

					});

				} else {
					console.log('het formulier werd niet correct ingevuld');
				}

			};
	
			initialize();

		}
	]);


})(angular, milla);

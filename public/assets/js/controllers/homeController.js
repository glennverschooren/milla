
(function HomeController(ng, milla) {
	'use strict';

	milla.controller('milla.HomeController', [
		'$document',
		'$window',
		'$rootScope',
		'$http',
		'$timeout',
		'$scope',
		'$sce',
		'SvgIcons',
		'SvgIconConfig',
		'lodash',
		function HomeController($document, $window, $rootScope, $http, $timeout, $scope, $sce, SvgIcons, SvgIconConfig, _) {



			/**
			 * Initialize -------------------------------------
			 * @return {[type]} [description]
			 */
			function initialize() {
			}

			$scope.productions = [
				{
					id: 'stuk',
					title: 'STUK',
					date: '2017',
					quote: $sce.trustAsHtml('STUK: Guerilla van waarheid tegen illusie'),
					body: $sce.trustAsHtml('Wil je de waarheid weten of ben je beter af met een illusie? In iedere levensfase krijg je die vraag wel eens voorgeschoteld. Soms kies je pertinent voor de waarheid, een ander keer liever niet en leef je gelukkig in de illusie. Vijf acteurs zitten met dezelfde vragen. Ze werken aan “STUK”, een theatervoorstelling die het ultieme antwoord moet geven. Zo komen ze terecht bij “de wilde eend” van Ibsen en “illusies voor gevorderden” van Maarten Boudry. Maar is theater maken zelf geen illusie?'),
					images: [
						{
							url: '/assets/img/gfxMilla/stuk/stuk-slider1.jpg'
						},
						{
							url: '/assets/img/gfxMilla/stuk/stuk-slider2.jpg'
						},
						{
							url: '/assets/img/gfxMilla/stuk/stuk-slider3.jpg'
						}
					],
					info: {
						colofon: $sce.trustAsHtml('<strong>Tekst en regie </strong> Bernard Soenens / <strong> Productie </strong> Theater Milla / <strong> Spel </strong> Cécile De Somer, Ellen De Smet, Louise Vrints, Thomas Lauwers en Jonathan Franck / <strong> Campagnebeeld </strong> Jade Sips / <strong> Visagie </strong> Jasmien Dieltiens'),
						playTime: [
							{
								id: "1",
								date: '21-09-2017',
								time: '20:00',
								venue: 'Zaal Zirkus Antwerpen'
							},
							{
								id: "2",
								date: '22-09-2017',
								time: '20:00',
								venue: 'Zaal Zirkus Antwerpen'
							},
							{
								id: "3",
								date: '23-09-2017',
								time: '20:00',
								venue: 'Zaal Zirkus Antwerpen'
							},
							{
								id: "4",
								date: '29-09-2017',
								time: '20:00',
								venue: 'Zaal Zirkus Antwerpen'
							},
							{
								id: "5",
								date: '30-09-2017',
								time: '20:00',
								venue: 'Zaal Zirkus Antwerpen'
							}
						],
						location: $sce.trustAsHtml("Zaal Zirkus: Zirkstraat 36, 2000 Antwerpen <br/> Ticketprijs: Basisprijs €10 / Studentenprijs: €7"),
						tickets: {
							priceInfo: [
								{
									age: 'Studentenprijs',
									value: '€7'
								},
								{
									age: 'Basisprijs',
									value: '€10'
								}
							],
							form: {
								pending: false,
								sections: [{
									title: 'Bestel tickets',
									fields: [{
										value: '',
										name: 'firstname',
										label: 'Voornaam',
										id: 'firstname',
										type: 'text',
										layout: {
											fieldClass: 'span-100 tablet--span-6-1 desktop--span-6-1'
										},
										attributes: {
											placeholder: ''
										},
										validation: {
											required: true,
											errorMessage: 'Gelieve een voornaam in te vullen'
										}
									},
									{
										value: '',
										id: 'lastname',
										label: 'naam',
										name: 'lastname',
										type: 'text',
										layout: {
											fieldClass: 'span-100 tablet--span-6-7 desktop--6-7'
										},
										attributes: {
											placeholder: ''
										},
										validation: {
											required: true,
											errorMessage: 'Gelieve een naam in te vullen'
										}
									},
									{
										value: '',
										id: 'ticketAmount1',
										label: 'Aantal tickets studentenprijs (€7)',
										name: 'ticketAmount1',
										type: 'text',
										layout: {
											fieldClass: 'span-100 tablet--span-6-1 desktop--span-6-1'
										},
										attributes: {
											placeholder: ''
										},
										validation: {
											required: false,
											validators: [{
												name: 'Digits'
											}],
											errorMessage: 'Gelieve een aantal in te vullen'
										}
									},
									{
										value: '',
										id: 'date',
										name: 'date',
										label: 'Datum',
										type: 'select',
										layout: {
											fieldClass: 'span-100 tablet--span-6-7 desktop--span-6-7'
										},
										attributes: {
											placeholder: ''
										},
										options: [
											{
												key: '21-09-2017',
												value: '21-09-2017'
											},
											{
												key: '22-09-2017',
												value: '22-09-2017'
											},
											{
												key: '23-09-2017',
												value: '23-09-2017'
											},
											{
												key: '29-09-2017',
												value: '29-09-2017'
											},
											{
												key: '30-09-2017',
												value: '30-09-2017'
											},
										],
										validation: {
											required: true,
											errorMessage: 'Gelieve een keuze te maken'
										}
									},
									{
										value: '',
										id: 'ticketAmount2',
										label: 'Aantal tickets basisprijs (€10)',
										name: 'ticketAmount2',
										type: 'text',
										layout: {
											fieldClass: 'span-100 tablet--span-6-1 desktop--span-6-1'
										},
										attributes: {
											placeholder: ''
										},
										validation: {
											required: false,
											validators: [{
												name: 'Digits'
											}],
											errorMessage: 'Gelieve een aantal in te vullen'
										}
									},
									{
										value: '',
										id: 'email',
										name: 'email',
										label: 'E-mail',
										type: 'text',
										layout: {
											fieldClass: 'span-100 tablet--span-6-7 desktop--span-6-7'
										},
										attributes: {
											placeholder: ''
										},
										validation: {
											required: true,
											errorMessage: "Gelieve een geldig e-mail adres in te vullen",
											validators: [{
												name: 'email'
											}]
										}
									}]
								}]
							}
						}

					}
				},
				{
					id: 'echo',
					title: 'ECHO',
					date: '2015',
					quote: $sce.trustAsHtml('Julia, Lady Macbeth, Lucretia en Ophelia.<br /> Wie zij zijn, doet er niet toe.<br /> Ze zoeken, worstelen en vechten met wat ze ooit geweest zijn, willen worden en denken dat ze zullen worden.<br /> Weggestoken, ondergedoken kabbelen ze voort en dat schuurt, beukt en scheurt hen in stukken.<br />'),
					body: $sce.trustAsHtml('ECHO is geïnspireerd op vier vrouwelijke personages uit de bekende verhalen van William Shakespaere. Zij krijgen iets wat hen jarenlang werd ontnomen: een stem. In hun twijfels, verlangens, driften en noden vertellen zij het verhaal over vier vrouwen die op zoek zijn naar hun plaats in de wereld. Voor deze voorstelling werkt theater Milla samen met Koor Notches, een sociaal-artistiek project waarin mensen met uiteenlopende achtergronden verenigd worden om samen te zingen.'),
					images: [
						{
							url: '/assets/img/gfxMilla/echo/echo-slider1.jpg'
						},
						{
							url: '/assets/img/gfxMilla/echo/echo-slider2.jpg'
						},
						{
							url: '/assets/img/gfxMilla/echo/echo-slider3.jpg'
						},
						{
							url: '/assets/img/gfxMilla/echo/echo-slider4.jpg'
						},
						{
							url: '/assets/img/gfxMilla/echo/echo-slider5.jpg'
						},
						{
							url: '/assets/img/gfxMilla/echo/echo-slider6.jpg'
						},
						{
							url: '/assets/img/gfxMilla/echo/echo-slider7.jpg'
						},
						{
							url: '/assets/img/gfxMilla/echo/echo-slider8.jpg'
						},
						{
							url: '/assets/img/gfxMilla/echo/echo-slider9.jpg'
						}
					],
					info: {
						colofon: $sce.trustAsHtml('<strong> spel en concept </strong> Liesje De Smet, Cécile De Somer, Jade Sips en Louise Vrints / <strong> dramaturgie en concept </strong> Eva Soenens/ <strong> kostuum </strong> Josephine De Jaegere / <strong> muziek </strong> Tine Joris en Peter Spaepen / <strong> koor </strong> Notches / <strong> techniek </strong> Mieke Scheirs / <strong> flyer </strong> Jade Sips / <strong> visagie </strong> Jasmien en Marjolein Dieltiens '),
						playTime: [
							{
								date: '9-10-2015',
								time: '20:00',
								venue: 'CarWash Theater Borgerhout'
							},
							{
								date: '10-10-2015',
								time: '20:00',
								venue: 'CarWash Theater Borgerhout'
							},
							{
								date: '16-10-2015',
								time: '20:00',
								venue: 'CarWash Theater Borgerhout'
							},
							{
								date: '17-10-2015',
								time: '20:00',
								venue: 'CarWash Theater Borgerhout'
							},
							{
								date: '18-10-2015',
								time: '14:00',
								venue: 'CarWash Theater Borgerhout'
							},
							{
								date: '15-01-2016',
								time: '20:30',
								venue: 'Stalteater, Oelegem'
							},
							{
								date: '16-01-2016',
								time: '20:30',
								venue: 'Stalteater, Oelegem'
							},
							{
								date: '23-01-2016',
								time: '20:30',
								venue: 'Stalteater, Oelegem'
							}
						],
						location: $sce.trustAsHtml('<p>CarWash Theater: Sergeyselsstraat 42, 2140 Borgerhout </br> Stalteater: Knodbaan 149b, 2520 Oelegem, reservatie: <a href="http://www.stalteater.be">Stalteater</a></p><p>Ticketprijs: Carwash Theater €8/€5(-26)</p>'),
						//sponsers: $sce.trustAsHtml('In samenwerking met Vila Cabral en Fameus vzw.<br /> Gerealiseerd met de steun van Stad Antwerpen.'),
						//poster: '/assets/img/gfxMilla/no-need-to-panic/flyer.jpg',
						// tickets: {
						// 	priceInfo: [
						// 		{
						// 			age: '-26j',
						// 			value: '€15'
						// 		},
						// 		{
						// 			age: '+26j',
						// 			value: '€20'
						// 		}
						// 	],
						// 	form: {
						// 		pending: false,
						// 		sections: [{
						// 			title: 'Bestel tickets',
						// 			fields: [{
						// 				value: '',
						// 				name: 'firstname',
						// 				label: 'Voornaam',
						// 				id: 'firstname',
						// 				type: 'text',
						// 				layout: {
						// 					fieldClass: 'span-100 tablet--span-6-1 desktop--span-6-1'
						// 				},
						// 				attributes: {
						// 					placeholder: ''
						// 				},
						// 				validation: {
						// 					required: true,
						// 					errorMessage: 'Gelieve een voornaam in te vullen'
						// 				}
						// 			},
						// 			{
						// 				value: '',
						// 				id: 'lastname',
						// 				label: 'naam',
						// 				name: 'lastname',
						// 				type: 'text',
						// 				layout: {
						// 					fieldClass: 'span-100 tablet--span-6-7 desktop--6-7'
						// 				},
						// 				attributes: {
						// 					placeholder: ''
						// 				},
						// 				validation: {
						// 					required: true,
						// 					errorMessage: 'Gelieve een naam in te vullen'
						// 				}
						// 			},
						// 			{
						// 				value: '',
						// 				id: 'ticketAmount1',
						// 				label: 'Aantal tickets -26j',
						// 				name: 'ticketAmount1',
						// 				type: 'text',
						// 				layout: {
						// 					fieldClass: 'span-100 tablet--span-6-1 desktop--span-6-1'
						// 				},
						// 				attributes: {
						// 					placeholder: ''
						// 				},
						// 				validation: {
						// 					required: false,
						// 					validators: [{
						// 						name: 'Digits'
						// 					}],
						// 					errorMessage: 'Gelieve een aantal in te vullen'
						// 				}
						// 			},
						// 			{
						// 				value: '',
						// 				id: 'date',
						// 				name: 'date',
						// 				label: 'Datum',
						// 				type: 'select',
						// 				layout: {
						// 					fieldClass: 'span-100 tablet--span-6-7 desktop--span-6-7'
						// 				},
						// 				attributes: {
						// 					placeholder: ''
						// 				},
						// 				options: [
						// 				{
						// 					key: '10-10-2015',
						// 					value: '10-10-2015'
						// 				},
						// 				{
						// 					key: '17-10-2015',
						// 					value: '17-10-2015'
						// 				},
						// 				{
						// 					key: '18-10-2015',
						// 					value: '18-10-2015'
						// 				}],
						// 				validation: {
						// 					required: true,
						// 					errorMessage: 'Gelieve een keuze te maken'
						// 				}
						// 			},
						// 			{
						// 				value: '',
						// 				id: 'ticketAmount2',
						// 				label: 'Aantal tickets +26j',
						// 				name: 'ticketAmount2',
						// 				type: 'text',
						// 				layout: {
						// 					fieldClass: 'span-100 tablet--span-6-1 desktop--span-6-1'
						// 				},
						// 				attributes: {
						// 					placeholder: ''
						// 				},
						// 				validation: {
						// 					required: false,
						// 					validators: [{
						// 						name: 'Digits'
						// 					}],
						// 					errorMessage: 'Gelieve een aantal in te vullen'
						// 				}
						// 			},
						// 			{
						// 				value: '',
						// 				id: 'email',
						// 				name: 'email',
						// 				label: 'E-mail',
						// 				type: 'text',
						// 				layout: {
						// 					fieldClass: 'span-100 tablet--span-6-7 desktop--span-6-7'
						// 				},
						// 				attributes: {
						// 					placeholder: ''
						// 				},
						// 				validation: {
						// 					required: true,
						// 					errorMessage: "Gelieve een geldig e-mail adres in te vullen",
						// 					validators: [{
						// 						name: 'email'
						// 					}]
						// 				}
						// 			}]
						// 		}]
						// 	}
						// }
					}
				},
				{
					id: 'noNeedToPanic',
					title: 'No Need To Panic',
					date: '2014',
					quote: $sce.trustAsHtml('Een man aanschouwt en stuurt.<br /> Een ander voert uit.<br /> Een vrouw bezwijkt en heeft lief.<br /> Een ander wacht. Hij wacht op een einde.<br /> En zij lijdt.<br /> Van die ondergang.'),
					body: $sce.trustAsHtml('‘No Need To Panic’ is een zintuiglijke performance die gerealiseerd is in opdracht van Fameus en Vila Cabral voor het jaarthema van 11.11.11. ‘voedselverspilling’. Geïnspireerd op de Griekse mythe ‘Theseus en de Minotaurus’ brengt Theater Milla haar interpretatie van deze hedendaagse problematiek. De mythe vertelt het verhaal van een machtig man, een werker, een verlangende vrouw en een wezen dat het daglicht niet mag zien. Een stuk over de mens die zich als God boven de wereld verheft en deze manipuleert tot hij hem niet meer in de hand heeft. Een atypische, prikkelende voorstelling waarbij je in de sprookjesachtige Oude Beurs te Antwerpen wordt rondgeleid en afdaalt van kille hoogtes tot in grimmige kelders.'),
					images: [
						{
							url: '/assets/img/gfxMilla/no-need-to-panic/nntp-slider1.jpg'
						},
						{
							url: '/assets/img/gfxMilla/no-need-to-panic/nntp-slider2.jpg'
						},
						{
							url: '/assets/img/gfxMilla/no-need-to-panic/nntp-slider3.jpg'
						},
						{
							url: '/assets/img/gfxMilla/no-need-to-panic/nntp-slider4.jpg'
						},
						{
							url: '/assets/img/gfxMilla/no-need-to-panic/nntp-slider6.jpg'
						}
					],
					info: {
						colofon: $sce.trustAsHtml('<strong> spel </strong> Ellen De Smet, Louise Vrints, Jade Sips, Jeroen Meylemans en Pieter Truyens / <strong> muziek </strong> Isa Jollings en Loni Cornelis / <strong> regie </strong> Eva Soenens en Ella Peeters / <strong> coaching </strong> Maarten Goffin / <strong> dans </strong> Jente Soenens, Lara Soenens, Roxane Nicasy, Tunke Lauriks, Johanne Ampe en Amber Paris / <strong> techniek </strong> Maarten Snoeck / <strong> promotie </strong> Maud Peeters en Jade Sips / <strong> foto\'s </strong> Rolf Gerlach / <strong> visagie </strong> Marjolein en Jasmien Dieltiens '),
						playTime: [
							{
								date: '6-11-2014',
								venue: 'Oude Beurs, Antwerpen'
							},
							{
								date: '7-11-2014',
								venue: 'Oude Beurs, Antwerpen'
							},
							{
								date: '8-11-2014',
								venue: 'Oude Beurs, Antwerpen'
							},
							{
								date: '9-11-2014',
								venue: 'Oude Beurs, Antwerpen'
							},
							{
								date: '13-11-2014',
								venue: 'Oude Beurs, Antwerpen'
							},
							{
								date: '14-11-2014',
								venue: 'Oude Beurs, Antwerpen'
							}
						],
						sponsers: $sce.trustAsHtml('In samenwerking met Vila Cabral en Fameus vzw.<br /> Gerealiseerd met de steun van Stad Antwerpen.'),
						poster: '/assets/img/gfxMilla/no-need-to-panic/flyer.jpg'/*,
						tickets: {
							priceInfo: [
								{
									age: '-26j',
									value: '€15'
								},
								{
									age: '+26j',
									value: '€20'
								}
							],
							form: {
								pending: false,
								sections: [{
									title: 'Bestel tickets',
									fields: [{
										value: '',
										name: 'firstname',
										label: 'Voornaam',
										id: 'firstname',
										type: 'text',
										layout: {
											fieldClass: 'span-100 tablet--span-6-1 desktop--span-6-1'
										},
										attributes: {
											placeholder: ''
										},
										validation: {
											required: true,
											errorMessage: 'Gelieve een voornaam in te vullen'
										}
									},
									{
										value: '',
										id: 'lastname',
										label: 'naam',
										name: 'lastname',
										type: 'text',
										layout: {
											fieldClass: 'span-100 tablet--span-6-7 desktop--6-7'
										},
										attributes: {
											placeholder: ''
										},
										validation: {
											required: true,
											errorMessage: 'Gelieve een naam in te vullen'
										}
									},
									{
										value: '',
										id: 'ticketAmount1',
										label: 'Aantal tickets -26j',
										name: 'ticketAmount1',
										type: 'text',
										layout: {
											fieldClass: 'span-100 tablet--span-6-1 desktop--span-6-1'
										},
										attributes: {
											placeholder: ''
										},
										validation: {
											required: false,
											validators: [{
												name: 'Digits'
											}],
											errorMessage: 'Gelieve een aantal in te vullen'
										}
									},
									{
										value: '',
										id: 'date',
										name: 'date',
										label: 'Datum',
										type: 'select',
										layout: {
											fieldClass: 'span-100 tablet--span-6-7 desktop--span-6-7'
										},
										attributes: {
											placeholder: ''
										},
										options: [{
											key: '15-06-2015',
											value: '15-06-2015'
										},
										{
											key: '16-06-2015',
											value: '16-06-2015'
										},
										{
											key: '17-06-2015',
											value: '17-06-2015'
										},
										{
											key: '18-07-2015',
											value: '18-07-2015'
										}],
										validation: {
											required: true,
											errorMessage: 'Gelieve een keuze te maken'
										}
									},
									{
										value: '',
										id: 'ticketAmount2',
										label: 'Aantal tickets +26j',
										name: 'ticketAmount2',
										type: 'text',
										layout: {
											fieldClass: 'span-100 tablet--span-6-1 desktop--span-6-1'
										},
										attributes: {
											placeholder: ''
										},
										validation: {
											required: false,
											validators: [{
												name: 'Digits'
											}],
											errorMessage: 'Gelieve een aantal in te vullen'
										}
									},
									{
										value: '',
										id: 'email',
										name: 'email',
										label: 'E-mail',
										type: 'text',
										layout: {
											fieldClass: 'span-100 tablet--span-6-7 desktop--span-6-7'
										},
										attributes: {
											placeholder: ''
										},
										validation: {
											required: true,
											errorMessage: "Gelieve een geldig e-mail adres in te vullen"

										}
									}]
								}]
							}
						}*/
					}
				},
				{
					id: 'orpheus',
					title: 'Orpheus',
					date: '2014',
					body: $sce.trustAsHtml('Orpheus is een bewerking van een van de bekendste liefdesgeschiedenissen uit de wereldliteratuur, over de zanger Orpheus die zijn geliefde, de nimf Euredyce verliest. Ze sterft en moet naar de onderwereld. Orpheus gaat haar halen en mag haar terug meenemen indien hij tijdens hun terugtocht niet achterom kijkt. Hij doet het toch. <br /><br />Dit is een verhaal. Over een liefde. Een verloren liefde. Tot twee maal toe. Maar toch omkeerbaar. Op een week tijd, maakten we onze eigen, fysieke en vooral muzikale bewerking van dit oude verhaal. We lieten ons inspireren door de unieke locatie: een Grieks amfitheater. <br /><br /> Theater Milla speelde deze voorstelling eenmalig op het theaterfestival Spots op West, in Westouter.'),
					images: [
						{
							url: '/assets/img/gfxMilla/orpheus/orpheus-slider1.jpg'
						},
						{
							url: '/assets/img/gfxMilla/orpheus/orpheus-slider2.jpg'
						},
						{
							url: '/assets/img/gfxMilla/orpheus/orpheus-slider3.jpg'
						},
						{
							url: '/assets/img/gfxMilla/orpheus/orpheus-slider4.jpg'
						},
						{
							url: '/assets/img/gfxMilla/orpheus/orpheus-slider5.jpg'
						}/*,
						{
							url: '/assets/img/gfxMilla/orpheus/orpheus-slider6.jpg'
						}*/
					],
					info: {
						colofon: $sce.trustAsHtml('<strong> concept, tekst en spel </strong> Jeroen Meylemans, Louise Vrints, Jade Sips en Eva Soenens'),
						playTime: [
							{
								date: '13-07-2014',
								venue: 'Amfitheater, Westouter'
							}
						],
						sponsers: $sce.trustAsHtml(' '),
						poster: '/assets/img/gfxMilla/orpheus/flyer.jpg',
					}
				},
				{
					id: 'reynaert',
					title: 'Reynaert.',
					date: '2014',
					quote: $sce.trustAsHtml('Over een vos en een leeuw.<br /> En nog een resem beesten.<br /> Over haat. Hartsgrondige haat. Wraaklust.<br /> Groter willen, hoger willen, sterker willen. Maar niet kunnen.<br /> Over een zondig leven. Over de mens. Over de mens in zijn dierlijke driften. Over de mens die dier is.<br /> Homo homini lupus est.'),
					body: $sce.trustAsHtml('Van den vos Reynaerde is een schets van een maatschappij die schijnbaar goed geordend en ‘menselijk’ is, maar in zijn fundament door wellust en eigenbelang wordt gedreven. Een verhaal waar \\‘slecht\\’ en \\‘goed\\’ op losse schroeven worden gezet en taal als machtsmiddel centraal staat. In lijn met onze vorige producties hebben we gezocht naar een voorstelling waar beeld en muziek een belangrijke rol spelen. We trachtten een universele kern binnen dit eeuwenoude verhaal te vinden, en bouwden die kern op met allerlei beelden, tekst en muziek, vanuit verschillende inspiratiebronnen. Trixie Whitley, Sol Lewitt, Ylvis, allerlei dans- en theatervoorstellingen, Julian Assange, Hamlet, Nina Simone,... Ze sluiten allemaal naadloos bij elkaar aan in onze Reynaert. Een muziektheatervoorstelling met straffe taal en krachtige lijven. Vol haat, wraaklust en driften. <br /><br />Reynaert. werd geselecteerd voor het festival AmateurToneelhuis, georganiseerd door Toneelhuis en Opendoek, in de Bourlaschouwburg te Antwerpen. Daarnaast werd het ook geselecteerd voor de 7e editie van Comedrama, Le festival international de théâtre, in Oujda, Marokko.'),
					images: [
						{
							url: '/assets/img/gfxMilla/reyneart/reyneart-slider1.jpg'
						},
						{
							url: '/assets/img/gfxMilla/reyneart/reyneart-slider2.jpg'
						},
						{
							url: '/assets/img/gfxMilla/reyneart/reyneart-slider3.jpg'
						},
						{
							url: '/assets/img/gfxMilla/reyneart/reyneart-slider5.jpg'
						},
						{
							url: '/assets/img/gfxMilla/reyneart/reyneart-slider7.jpg'
						},
						{
							url: '/assets/img/gfxMilla/reyneart/reyneart-slider8.jpg'
						},
						{
							url: '/assets/img/gfxMilla/reyneart/reyneart-slider9.jpg'
						},
						{
							url: '/assets/img/gfxMilla/reyneart/reyneart-slider10.jpg'
						},
						{
							url: '/assets/img/gfxMilla/reyneart/reyneart-slider11.jpg'
						}
					],
					info: {
						colofon: $sce.trustAsHtml('<strong> spel </strong> Josephine De Jaegere, Ellen De Smet, Liesje De Smet, Jonathan Franck, Kirkland Klaes, Ella Peeters, Louise Vrints / <strong> regie </strong> Eva Soenens / <strong> muziek </strong> Pieter Truyens, Jade Sips / <strong> tekst </strong> Suzan Hogenbirk / <strong> dramaturgie </strong> Mieke Scheirs / <strong>kostuum</strong> Ella Peeters, Karin Nuñez De Fleurquin / <strong>scenografie</strong> Katrien Geebelen / <strong>techniek</strong> Mieke Scheirs, Suzan Hogenbirk / <strong>decorbouw</strong> Alexander Bellens / <strong>promotie</strong> Vincent Vrints / <strong> foto\'s </strong> Rolf Gerlach /  <strong>productie</strong> Anneleen Bouving / <strong>visagie</strong> Marjolein en Jasmien Dieltiens'),
						playTime: [
							{
								date: '28-02-2014',
								venue: 'Zaal Zirkus, Antwerpen'
							},
							{
								date: '1-03-2014',
								venue: 'Zaal Zirkus, Antwerpen'
							},
							{
								date: '2-03-2014',
								venue: 'Zaal Zirkus, Antwerpen'
							},
							{
								date: '6-03-2014',
								venue: 'Zaal Zirkus, Antwerpen'
							},
							{
								date: '7-03-2014',
								venue: 'Zaal Zirkus, Antwerpen'
							},
							{
								date: '8-03-2014',
								venue: 'Zaal Zirkus, Antwerpen'
							},
							{
								date: '16-04-2014',
								venue: 'Toneelhuis, Antwerpen'
							},
							{
								date: '17-02-2014',
								venue: 'Comedrama, Oujda'
							}
						],
						sponsers: $sce.trustAsHtml('Gerealiseerd met de steun van Stad Antwerpen.'),
						poster: '/assets/img/gfxMilla/reyneart/flyer.jpg',
					},

				},
				{
					id: 'terugNaarWaarDan',
					title: 'Terug naar waar dan ',
					date: '2013',
					quote: $sce.trustAsHtml('<br />Fantasie is beter dan de werkelijkheid. <br />In de meeste gevallen dan toch'),
					body: $sce.trustAsHtml('Na de dood van haar moeder vlucht Elise in haar boeken. Wanneer ze door haar zussen gedwongen wordt uit haar eigen wereldje te komen, neemt haar leven een mysterieuze wending. Alles lijkt ineens op zijn plaats te vallen. Haar leven is weer zoals het zou moeten zijn. Elise laat zich meevoeren door dit gelukzalig gevoel, maar verliest langzaam de grip op haar bestaan. En wie weet dan nog wat fantasie en wat werkelijkheid was? Terug naar waar dan is een voorstelling over wat er gebeurt als je je overgeeft aan je droombeeld, dat toch niet zo ideaal blijkt te zijn als je dacht. Over de weg terugvinden, die langzaam verdwijnt als je er te diep in verstrikt raakt.'),
					images: [
						{
							url: '/assets/img/gfxMilla/terug-naar-waar-dan/tnwd-slider1.jpg'
						},
						{
							url: '/assets/img/gfxMilla/terug-naar-waar-dan/tnwd-slider2.jpg'
						},
						{
							url: '/assets/img/gfxMilla/terug-naar-waar-dan/tnwd-slider3.jpg'
						},
						{
							url: '/assets/img/gfxMilla/terug-naar-waar-dan/tnwd-slider4.jpg'
						},
						{
							url: '/assets/img/gfxMilla/terug-naar-waar-dan/tnwd-slider5.jpg'
						},
						{
							url: '/assets/img/gfxMilla/terug-naar-waar-dan/tnwd-slider6.jpg'
						},
						{
							url: '/assets/img/gfxMilla/terug-naar-waar-dan/tnwd-slider7.jpg'
						},
						{
							url: '/assets/img/gfxMilla/terug-naar-waar-dan/tnwd-slider8.jpg'
						},
						{
							url: '/assets/img/gfxMilla/terug-naar-waar-dan/tnwd-slider9.jpg'
						},
						{
							url: '/assets/img/gfxMilla/terug-naar-waar-dan/tnwd-slider10.jpg'
						},
						{
							url: '/assets/img/gfxMilla/terug-naar-waar-dan/tnwd-slider11.jpg'
						}
					],
					info: {
						colofon: $sce.trustAsHtml('<strong>spel</strong> Josephine De Jaegere, Ellen De Smet, Jeroen Meylemans, Ella Peeters, Eva Soenens, Jens Van den Eynde, Elisabeth-Ann Vandenwyngaerden en Louise Vrints / <strong>regie</strong> Liesje De Smet / <strong>script</strong> Suzan Hogenbirk / <strong>dramaturgie</strong> Mieke Scheirs / <strong>kostuum</strong> Zsuzsanna Bossányi / <strong>decor</strong> Katrien Geebelen  / <strong>muziek</strong> F.J. Laloosh / <strong>techniek</strong> Matthijs Kremers en Roeland Vandebriel / <strong>promotie</strong> Vincent Vrints / <strong> foto\'s </strong> Matthijs Kremers / <strong>productie</strong> Josephine De Jaegere en Mieke Scheirs'),
						playTime: [
							{
								date: '18-04-2013',
								venue: 'De Feniks, Mortsel'
							},
							{
								date: '19-04-2013',
								venue: 'De Feniks, Mortsel'
							},
							{
								date: '20-04-2013',
								venue: 'De Feniks, Mortsel'
							},
							{
								date: '3-05-2013',
								venue: 'Rataplan, Borgerhout'
							},
							{
								date: '4-05-2013',
								venue: 'Rataplan, Borgerhout'
							}
						],
						poster: '/assets/img/gfxMilla/terug-naar-waar-dan/flyer.jpg',
						sponsers: $sce.trustAsHtml('Met dank aan \'t Werkhuys en Rataplan vzw.<br /> Gerealiseerd met de steun van Provincie Antwerpen en District Borgerhout.')
					}
				},
				{
					id: 'zonderMeer',
					title: 'Zonder Meer',
					date: '2009',
					quote: $sce.trustAsHtml('Eén meisje.<br />Met behulp van een madeliefje bepaalt ze haar eigen dromen.<br />Op zoek naar geluk. Alleen. En toch samen.'),
					body: $sce.trustAsHtml('De productie van 2009 stond in het teken van experimenteren met beeld en muziek. ‘Zonder meer’, een kortfilm van zeven minuten, was hiervan het resultaat. zintuiglijke, muzikale en beeldende voorstellingen die het publiek willen grijpen en beroeren.'),
					images: [
						{
							url: '/assets/img/gfxMilla/zonder-meer/zm-slider5.jpg'
						},
						{
							url: '/assets/img/gfxMilla/zonder-meer/zm-slider6.jpg'
						},
						/*,
						{
							url: '/assets/img/gfxMilla/terug-naar-waar-dan/tnwd-slider6.jpg'
						}*/
					],
					info: {
						colofon: $sce.trustAsHtml('<strong>spel en concept</strong>  Josephine De Jaegere, Liesje De Smet en Louise Vrints / <strong>regie en concept</strong> Indra Van Hoorick / <strong>muziek</strong> Boris Uytterheagen / <strong>camera</strong> Roeland Vandebriel / <strong>camera-assistentie</strong> Matthijs Kremers en Joris Rijken / <strong>montage</strong> Mikael Wastyn /<strong>promotie</strong> Vincent Vrints / <strong>productie</strong> Theater Milla'),
						playTime: [
							{
								date: '17-05-2009',
								venue: 'Filmhuis Klappei, Antwerpen'
							},
							{
								date: '18-05-2009',
								venue: 'STUK, Leuven'
							}
						],
						poster: '/assets/img/gfxMilla/zonder-meer/flyer.jpg',
						sponsers: $sce.trustAsHtml('Gerealiseerd met de steun van Stad Antwerpen')
					}
				},
				{
					id: 'gekust',
					title: 'Gekust',
					date: '2008',
					quote: $sce.trustAsHtml('Nooit ben ik terug naar zee gegaan.<br />Ik krioelde in het zand van mijn fouten.<br />Ik doolde op het droge.<br />Ik haatte de wind.<br />Ik haatte alle mannen.<br />Ik droomde dat ik een prinses was.<br />Ik vloog weg van mezelf'),
					body: $sce.trustAsHtml('‘GeKust’ is een multimediale voorstelling, waarin naast tekst en spel ook filmbeelden, beweging en muziek een centrale rol spelen. De voorstelling is een allegorie over verdrinken in leven en herinneren, in fantasie en waanzin. De vier personages leven tussen verleden en heden, maar willen allemaal een andere kant uit. Hoelang kunnen ze zich hierachter nog verschuilen, of zullen ze ooit de waarheid onder ogen moeten zien? <br /> <br /> ‘GeKust’ vormt een mooi gestructureerd geheel dat het publiek laat verdrinken in de chaotische dromerige wereld van een meisje dat niet meer weet hoe ze verder moet gaan met haar leven.'),
					images: [
						{
							url: '/assets/img/gfxMilla/gekust/gekust-slider3.jpg'
						},
						{
							url: '/assets/img/gfxMilla/gekust/gekust-slider4.jpg'
						}
					],
					info: {
						colofon: $sce.trustAsHtml('<strong>spel en concept</strong> Josephine De Jaegere, Liesje De Smet, Indra Van Hoorick en Louise Vrints /<strong>regie en tekst</strong> Christoffel Hendrickx / <strong>muziek</strong> Boris Uytterheagen / <strong>techniek</strong> Roeland Vandebriel / <strong>promotie</strong> Vincent Vrints / <strong>productie</strong> Theater Milla'),
						playTime: [
							{
								date: '2-04-2008',
								venue: 'Fakkeltheater, Antwerpen'
							},
							{
								date: '3-04-2008',
								venue: 'Fakkeltheater, Antwerpen'
							},
							{
								date: '11-04-2008',
								venue: 'Vzw Sering, Borgerhout'
							},
							{
								date: '12-04-2008',
								venue: 'Vzw Sering, Borgerhout'
							}
						],
						poster: '/assets/img/gfxMilla/gekust/flyer.jpg',
						sponsers: $sce.trustAsHtml('Gerealiseerd met de steun van Provincie Antwerpen en District Borgerhout.')
					}
				},
				{
					id: 'theaterMilla',
					title: 'Theater Milla',
					date: '',
					body: $sce.trustAsHtml('Theater Milla is een jong theatergezelschap uit Antwerpen dat in 2007 werd opgestart door vier enthousiaste jongeren. Enkele jaren later is Milla uitgegroeid tot een divers en multidisciplinair team dat haar creatief talent ten volle benut bij elke productie. Acteren, regisseren, teksten schrijven, livemuziek, decor en kostuums, alles doen we zelf. Dit leidt tot een grote bron van creativiteit, inspiratie en ervaring. De bezetting wisselt elke productie, wat voor een grote dynamiek zorgt. Inhoudelijk zoeken we steeds naar prikkelende thema’s die een breed publiek aanspreken en kiezen we voor een variëteit in teksten: van repertoire tot zelfgeschreven teksten. Het resultaat hiervan zijn zintuiglijke, muzikale en beeldende voorstellingen die het publiek willen grijpen en beroeren.'),
					images: [
						{
							url: '/assets/img/gfxMilla/theater-milla/milla-slider1.jpg'
						},
						{
							url: '/assets/img/gfxMilla/theater-milla/milla-slider2.jpg'
						},
						{
							url: '/assets/img/gfxMilla/theater-milla/milla-slider3.jpg'
						},
						{
							url: '/assets/img/gfxMilla/theater-milla/milla-slider4.jpg'
						},
						{
							url: '/assets/img/gfxMilla/theater-milla/milla-slider5.jpg'
						}
					]
				}
			];


			//
			// SCOPE EVENTS ----------------------------------------
			//


			initialize();

		}
	]);


})(angular, milla);

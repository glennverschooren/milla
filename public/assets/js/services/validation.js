(function ValidationService(ng, milla) {
	'use strict';

	// Provide information about the current user.
	milla.service('ValidationService', [
		'Helper',
		'lodash',
		function ValidationService(Helper, _) {


			//
			// VALIDATE INPUT FIELD ----------------------------------
			// 
			// Validation object 
			// value 
			//

			function validateInputField(validation, value, type) {

				var validators = {
					required: isRequired,
					date: checkDate,
					validators: checkValidator
				};

				// Loop trough all validators  
				var checks = {};
				var validator = '';
				var errorMessage = '';

				// validate
				// return true ore fals 
				for (validator in validators) {
					checks[validator] = validators[validator](validation[validator], value, type);

					if (!checks[validator]) {
						break;
					}
				}

				//
				// Check if the result was true or falsy ------------------- 
				//
				for (var check in checks) {
					if (!checks[check]) {
						// check 
						validation.errorMessage = validation.errorMessage !== undefined ? validation.errorMessage : '';
						if (validation[check] !== undefined) {
							errorMessage = validation[check].errorMessage !== undefined ? validation[check].errorMessage : validation.errorMessage;
						}
						else
						{
							errorMessage = validation.errorMessage;
						}
						return {valid: false, message: errorMessage};
					}
				}

				return {valid: true};
			}


			//
			// CHECK DATA ---------------------------------------------
			// check id date is valid
			//

			function checkDate(validator, value, type) {

				if (type !== 'date' && type !== 'datetime') {
					return true;
				}

				var dateNow = new Date(Date.now());
				dateNow.setDate(dateNow.getDate() + 2);

				dateNow = Helper.date.formatDateyymmdd(dateNow);
				var dmy = value.split("/");
				var date = new Date(dmy[2], dmy[1] - 1, dmy[0]);
				var state = false;

				state = (date > dateNow);
				return state;
			}

			/**
             * @name  checkEmail -------------------------------------
             * @param  {String} value
             * @param  {Object} scope
             * @param  {Object} element
             * @param  {object} attrs
             * @param  {Object} param
             * @return {Boolean}
             */
			function checkmail(value) {
				 if (value === undefined || value === null || value === '') {
                    return true;
                }
                // check match
                if (typeof value === 'string') {
                    var validatePattern = checkMatch(value, {
                        pattern: /\S+@\S+\.\S+/
                    });
                     // if the pattern is true and the length of the value is less than 100
                    if (validatePattern === true && value.length <= 100) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    console.warn('email validation expect a string as value');
                    return true;
                }
			}


			/**
             * @name  check match -------------------------------------
             * @description check if a value is matching a given pattern
             * The pattern can be a string or a RegExp javascript object
             * @param  {String/RegExp} value
             * @param  {Object} scope
             * @param  {Object} element
             * @param  {object} attrs
             * @param  {Object} param
             * @return {Boolean}
             */
            function checkMatch(value, param) {
                var regExp,
                    match,
                    pattern = param.pattern;


                if (pattern) {
                    if (value === null) {
                        return false;
                    }
                    if (typeof pattern === 'string') {
                        // if the pattern has a / on index 0
                        // remove it from the pattern
                        if (pattern.indexOf('\/') === 0) {
                            pattern = pattern.substring(1, pattern.length);
                        }
                        // if the pattern has a / on the last index
                        // remove it from the pattern
                        if (pattern.lastIndexOf('\/') === pattern.length - 1) {
                            pattern = pattern.substring(0, pattern.length - 1);
                        }
                        regExp = new RegExp(pattern);
                    }
                    else if (pattern instanceof RegExp) {
                        regExp = pattern;
                    }

                    match = value.match(regExp);
                    return (match !== null);
                }
            }


			//
			// IS REQUIRED --------------------------------------------
			// check if a field is resuired and filled in 
			//

			function isRequired(validator, value, type) {
				
				if (validator !== undefined && validator) {
					var check = evaluateRequired(value);
					return check;
				}

				return true;
			}


			//
			// EVALUATE DIGITS --------------------------------------------
			// check if the value is a number
			//

			function evaluateDigits(val) {

				var regExp = new RegExp('^[0-9]+$');
				var mtch = val && val.match(regExp);
				return (mtch !== null);
			}


			/**
			*
			* CHECK VALIDATOR ---------------------------------------------
			*
			**/
			function checkValidator(validation, value) {
				var result = true;
				if (validation !== undefined) {
					for (var v in validation) {
						var validator = validation[v];
						switch (validator.name) {

						case 'Digits':
							if (!evaluateDigits(value)) {
								result = false;
							}
							break;
						case 'email':
							if (!checkmail(value)) {
								return false;
							}
							break;
						}
					}
				}

				return result;
			}




			//
			// EVALUATE REQUIRED --------------------------------------
			//

			function evaluateRequired(value) {

				if (typeof value === 'boolean') {
					return value;
				} else {
					if (value === undefined || value === null) {
						return false;
					}
					else {
						if (value.toString().length === 0) {
							return false;
						}
						return true;
					}
				}
			}


			return {
				validateInput: validateInputField
			};

		}
	]);
})(angular, milla);

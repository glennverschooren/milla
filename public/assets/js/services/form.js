(function FormService(ng, milla) {
	'use strict';

	milla.service('formService', [
		'lodash',
		'$resource',
		'$http',
		function FormService(_, $resource, $http) {


	

			var formResource = $resource('/ticket_request', {
				
			}, {
				sendForm: {
					method: "POST",
					isArray: false
				}
			});

			//
			// GET FORM RESULTS ----------------------------------------
			// get all form results!
			//
			
			function getFormResults(form) {

				var validFormData = {};
				if (form) {
					var sections = form.sections || [];
					for (var sectionIndex in sections) {
						var fields = sections[sectionIndex].fields || [];
						_.assign(validFormData, getFieldValues(fields));
					}
				}

				return validFormData;
				
			}

			//
			// GET FIELDS VALUES -------------------------------------
			// go trough all fields and set the key value pair
			//

			function getFieldValues(fields) {
				var returnFields = {};
				for (var i = 0; i < fields.length; i = i + 1) {

					var currentField = fields[i];
					var currentFieldName = currentField.name;
					returnFields[currentFieldName] = currentField.value;
				}

				return returnFields;
			}


			function sendForm(form, callback, error) {

				var formResults = getFormResults(form);
				var qry;
				qry = formResource.sendForm(formResults).$promise;

				qry.then(function onSuccess(response) {
					if (callback) {
						callback(response);
					}
				},
				function onError(err) {
					if (error) {
						error(err);
					}
				});

			}


			return {
				sendForm: sendForm
			};

			
		}
	]);

})(angular, milla);

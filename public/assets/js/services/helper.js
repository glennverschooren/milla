(function HelperService(ng, milla) {
	'use strict';

	milla.service('Helper', [
		'lodash',
		function Helper(_) {


			/**
			*
			* FUNCTION: PARSE DATE ------------------------
			* - parse string to date 
			*
			**/
			
			function parseDate(date) {
				var dateObj = Date.parse(date);

				if (dateObj === null) {
					dateObj = new Date(date);
				}

				return validateDate(dateObj) ? dateObj : null;
			}

			/**
			*
			* FUNCTION: VALIDATE DATE -----------------------
			* - check for a valid date 
			*
			**/
			
			function validateDate(date) {
				//return !date ? false : !isNaN(date.getTime());
				return false;
			}

			/**
			*
			* FUNCTION: FORMAT DATE YY MMM DD ---------------
			*
			**/
			
			function formatDateyymmdd(dateIn)  {

				var year = dateIn.getFullYear();
				var month = dateIn.getMonth(); // getMonth() is zero-based
				var day = dateIn.getDate();

				return new Date(year, month, day);
			}
			

			/**
			*
			* PUBLIC API: -----------------------------------
			*
			**/
			

			return {
				date: {
					parse: parseDate,
					formatDateyymmdd: formatDateyymmdd
				}
			};

		}
	]);

})(angular, milla);

//
// -------------------------------------------------------------------------
// GRUNT CONFIG
// http://gruntjs.com/api/grunt.config
//

module.exports = function(grunt) {

	var config = {
		development: 'assets',
		timestamp: '1'
	};


	require('load-grunt-tasks')(grunt);
	grunt.initConfig({
		config: config,
		pkg: grunt.file.readJSON('package.json'),


		/**
		 * -----------------------------------------------------------------
		 * JSHINT
		 * See .jshintrc for confguration options
		 * https://github.com/gruntjs/grunt-contrib-jshint
		 * http://www.jshint.com/docs/options/
		 */
		jshint: {
			options: {
				jshintrc: '.jshintrc',
				reporter: require('jshint-stylish')
			},
			all: [
				'<%= config.development %>/js/**/*.js',
				'!<%= config.development %>/js/libs/**/*.js'
			]
		},
				
		/**
		 * -----------------------------------------------------------------
		 * COMPILE SASS & CLEAN-UP VENDOR PREFIXES
		 * https://github.com/gruntjs/grunt-contrib-compass
		 */
		compass: {  // TASK
			options : {
				bundleExec: true,
				config: '<%= config.development %>/config.rb',
				force: true
			},

			dev : {
				options: { // Target options
					sassDir: ['<%= config.development %>/stylesheets/sass'],
					cssDir: ['<%= config.development %>/stylesheets/css']
				}
			}
		},

		/**
		 * ------------------------------------------------------------------
		 * WATCH FOR CHANGES AND UPDATE 
		 * https://github.com/gruntjs/grunt-contrib-watch
		 */
		watch: {
			compass: {
				files: ['<%= config.development %>/stylesheets/sass/{,*/}{,*/}{,*/}{,*/}{,*/}*.{scss,sass}'],
				tasks: ['compass:dev']
			}

		},

		/**
		 *
		 * CLEAN PREVIOUS BUILDS --------------------------------------------
		 * We will clean up all minified files before an new build
		 * we remove all temp folders after a successful build
		 *
		 * https://github.com/gruntjs/grunt-contrib-clean
		 */
		clean: {
			prebuild: {
				src: [
					'<%= config.development %>/js/milla-*.js'
				]
			},
			postbuild: {
				src: ['grunt-tmp']
			}
		},

		/**
		 * ------------------------------------------------------------------
		 * CSSMIN 
		 * https://github.com/gruntjs/grunt-contrib-cssmin
		 */
		cssmin: {
			dist: {
				files: {
					'<%= config.development %>/stylesheets/css/main.min.css': [
						'<%= config.development %>/stylesheets/css/main.css'
					]
				}
			}
		},

		/**
		 * -------------------------------------------------------------------
		 * CONCAT 
		 * Concat all javascript files and store them in a .tmp folder
		 * https://github.com/gruntjs/grunt-contrib-concat
		 */
		concat: {
			options: {
				separator: ';'
			},
			dist: {
				src: [
					'<%= config.development %>/js/**/*.js'
				],
				dest: 'grunt-tmp/milla.js'
			},
			vendor: {
				src: [
					'<%= config.development %>/libs/**/*.js',
					'!<%= config.development %>/libs/_*/**/*.js',
					'!<%= config.development %>/libs/*.js',
					'!<%= config.development %>/libs/**/_*/*.js',
					'!<%= config.development %>/libs/**/_*/**/*.js'
				],
				dest: '<%= config.development %>/js/milla-vendors-<%= config.timestamp %>.min.js'
			},
		},

		/**
		 * -------------------------------------------------------------------
		 * STRIP ALL JS FILES 
		 * Remove all console and debug commands
		 * https://github.com/jsoverson/grunt-strip
		 */
		strip: {
			dist: {
				src: 'grunt-tmp/milla.js',
				dest: 'grunt-tmp/milla-stripped.js',
				options: {
					nodes: [
						'console.log',
						'console.warn',
						'console.error',
						'console.group',
						'console.groupEnd',
						'console.info',
						'console.groupCollapsed',
						'debug'
					]
				}
			}
		},

		/**
		 * -------------------------------------------------------------------
		 * UGLIFY ALL JS FILES 
		 * https://github.com/gruntjs/grunt-contrib-uglify
		 */
		uglify: {
			options: {
				mangle: false
			},
			dist: {
				files: {
					'<%= config.development %>/js/milla-<%= config.timestamp %>.min.js': ['grunt-tmp/milla-stripped.js']
				}
			}
		}


	});

	grunt.registerTask('development', ['jshint', 'compass:dev', 'watch']);

	grunt.registerTask('default', ['development']);

	grunt.registerTask('build', [
		'clean:prebuild',
		'cssmin',
		'concat',
		'strip',
		'uglify',
		'clean:postbuild'
	]);
};

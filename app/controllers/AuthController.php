<?php

Class AuthController Extends BaseController {


	//
	// LOGIN ----------------------------------------------------
	// check username and password 
	// if there is a match create a new token store it in the database 
	// return the user object to the client
	//
	public function login() {

		if(Auth::attempt(array('username' => Input::json('username'), 'password' => Input::json('password')))){

			if(Auth::check()) {
  				$authToken = AuthToken::create(Auth::user());
  				$publicToken = AuthToken::publicToken($authToken);

  				$data = array(
  					"success" => true,
  					"user" => Auth::user(),
  					"token" => $publicToken,
  				);
			}
			return Response::json($data, 200);
		} else {
			return Response::json(array('flash' => 'Invalid email ore password'));
		}
	}

	//
	// USER LOGOUT ------------------------------------------------
	//

	public function logout() {
		Auth::logout();
		return Response::json(array('flash' => 'logout'));

	}

	//
	// GET USER ----------------------------------------------------
	//

	public function getUser() {

		if(Auth::check()) {

			$id = Auth::id();

			$token = DB::select('select public_key from ta_auth_tokens where auth_identifier = ' . $id, array(1));
		
			$data = array(
				"success" => true,
				"user" => Auth::user(),
				"token" => $token
			);

			return Response::json($data, 200);
		} else {
			$message = array(
				"message" => 'No user was logged in'
			);
			return Response::json($message, 401);
		}
	}
} 
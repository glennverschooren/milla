<?php

class TicketController extends BaseController {


    /**
     * Calculate total price -------------------------
     * @return [type] [description]
     */
    function calculateTotalPrice($amountPlus, $amountMin, $price) {

        $total = array(
            'totalAmountPlus26' => 0,
            'totalAmountMin26' => 0,
            'totalAmount' => 0
        );

        $total['totalAmountPlus26'] = ($amountPlus * $price['26+']);
        $total['totalAmountMin26'] = ($amountMin * $price['26-']);
        $total['totalAmount']= $total['totalAmountPlus26'] + $total['totalAmountMin26'];
        return $total;
    }

	/**
	 * Send email from contact form
	 *
	 * @return void
	 */
	protected function sendTicketForm()
	{

		$data = array (
            'fields' => array (
                'data' => array(
                    'Voornaam' => Input::json('firstname'),
                    'Naam' => Input::json('lastname'),
                    'Aantal-26j' => Input::json('ticketAmount1'),
                    'Aantal+26j' => Input::json('ticketAmount2'),
                    'Datum' => Input::json('date'),
                    'E-mail' => Input::json('email'),
                ),
                'price' => array()
            )
        );
        $user = array (
            'email'=>'theatermilla@gmail.com',
        	//'email'=>'verschoorenglenn@gmail.com',
			'name'=>'Milla aanvraag tickets'
        );

        // prive tickets--------------------------------
        $price = array (
            '26-' => '7',
            '26+' => '10'
        );

        $data['fields']['price'] = $this->calculateTotalPrice($data['fields']['data']['Aantal+26j'], $data['fields']['data']['Aantal-26j'], $price);

		Mail::send('emails.ticket', $data, function($message) use ($user)
		{
			$message->from('info@theater-milla.be', 'Theater Milla');
			$message->to($user['email'], $user['name'])->subject('Aanvraag tickets Milla');
			return 'success';
		});

        Mail::send('emails.ticketReply', $data, function($message) use ($data)
        {
            $message->from('info@theater-milla.be', 'Theater Milla');
            $message->to($data['fields']['data']['E-mail'], $data['fields']['data']['Voornaam'])->subject('Aanvraag tickets Theater Milla');
            return 'success';
        });
	}
}

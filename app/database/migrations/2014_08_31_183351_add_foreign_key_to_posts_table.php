<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToPostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('posts', function(Blueprint $table){

			$table->unsignedInteger('menus_id');
			$table->foreign('menus_id')->references('id')->on('menus')->onDelete('cascade')->onUpdate('cascade');
	
		});
	}
		

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		//$table->dropForeign('posts_menus_id_foreign');
	}

}



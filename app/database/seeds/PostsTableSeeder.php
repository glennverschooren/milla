<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder {

	public function run()
	{	

        DB::table('posts')->delete();
		$faker = Faker::create();


		foreach(range(1, 5) as $index)
		{
			Post::create([

				'postTitle' => $faker->sentence($nbWords = 3),
				'image' => $faker->imageUrl($width = 812, $height = 250),
				'date' => $faker->date,
				'body' => $faker->paragraph($nbSentences = 70),
				'menus_id' => $index,
			]);
		}
	}

}

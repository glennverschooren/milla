<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{	

        DB::table('users')->delete();
		$faker = Faker::create();
		User::create([
			'username' => 'glennverschooren',
			'email' => 'glenn.verschooren@boulevart.be',
			'password' => Hash::make('admin')
		]);
		
	}

}

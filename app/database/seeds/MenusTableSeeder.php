<?php

// Composer: "fzaninotto/faker": "v1.4.0"
use Faker\Factory as Faker;

class MenusTableSeeder extends Seeder {

	public function run()
	{	

        DB::table('menus')->delete();
		$faker = Faker::create();


		foreach(range(1, 5) as $index)
		{
			Menu::create([
				'id' => $index,
				'title' => $faker->sentence($nbWords = 3),
				'link' => $faker->sentence($nbWords = 2)
			]);
		}
	}

}
